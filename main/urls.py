from django.conf.urls import patterns, url
from django.contrib.auth.views import logout
from main import views

urlpatterns = patterns('', 
    url( r'^login', views.login_view, name='login'),
    url(r'^logout', logout, {'next_page' : '/'}, name='logout'),
    url(r'^signin', views.signin, name='signin'),
    url(r'^settings', views.settings, name='settings'),
    url(r'^ask', views.ask, name='ask'),
    url(r'^question/(?P<question_id>\d+)/(?P<page_n>\d+)$', views.question, name='question'),
    url(r'^hellodjango$', views.hellodjango, name='hellodjango'),
    url(r'^likequestion', views.like_question, name='like_question'),
    url(r'^likeanswer', views.like_answer, name='like_answer'),
    url(r'^rightanswer', views.right_answer, name='right_answer'),
    url(r'^((?P<sort_t>[^/]+)?)((/(?P<page_n>\d+))?)((/(?P<tag>[^/]+))?)/?$', views.index, name='index'),
)
