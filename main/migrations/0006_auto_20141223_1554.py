# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_remove_profile_avatar_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='answerlike',
            name='type',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='questionlike',
            name='type',
            field=models.IntegerField(default=1),
            preserve_default=True,
        ),
    ]
