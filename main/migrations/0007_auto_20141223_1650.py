# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20141223_1554'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='date_added',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
