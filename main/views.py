from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from collections import OrderedDict
import random
import json
from main.models import *
from main.forms import *


def getBestTags():
    return Tag.objects.all().order_by('rating').reverse()[:10]


def getBestUsers():
    return Profile.objects.all().order_by('rating').reverse()[:10]


def calc_entires(paginator, page_n):
    if len(paginator.page_range) > 9:
        if page_n < 1 or page_n > len(paginator.page_range) - 2:
            entires = OrderedDict.fromkeys(paginator.page_range[:3] + paginator.page_range[len(paginator.page_range) - 3:])
        else:
            entires = OrderedDict.fromkeys(paginator.page_range[:3] + paginator.page_range[page_n - 2:page_n + 1] +  paginator.page_range[len(paginator.page_range) - 3:])
        entires = list(entires)
        if page_n > 5:
            entires = entires[:3] + ["..."] + entires[3:]
        if page_n < len(paginator.page_range) - 4:
            entires = entires[:len(entires) - 3] + ["..."] + entires[len(entires) - 3:]
    else:
        entires = paginator.page_range
    return entires


def index(request, sort_t, tag, page_n):
    onPage = 10
    if sort_t == 'popular':
        sort_column = 'rating'
    else:
        sort_column = 'date_added'
        sort_t = 'recent'

    if tag is not None and tag is not "":
        questions = Tag.objects.get(name=tag).question_set.order_by(sort_column).reverse()
    else:
        questions = Question.objects.order_by(sort_column).reverse()
        tag=""

    paginator = Paginator(questions, onPage)
    if page_n is None:
        page_n = 1
    else:
        page_n = int(page_n)
    entires = calc_entires(paginator, page_n)
    return render(request, 'index.html', {
        'sort_t': sort_t,
        'tag': tag,
        'tags': getBestTags(), 'logged_in' : bool(random.randrange(1)),
        'questions': paginator.page(page_n),
        'entries': entires,
        'bests': getBestUsers(),
    })


def question(request, question_id, page_n):
    onPage = 10
    question = Question.objects.get(pk=question_id)
    answers = question.answer_set.order_by('-is_right', '-rating')
    paginator = Paginator(answers, onPage)
    if page_n is None:
        page_n = 1
    else:
        page_n = int(page_n)
    entires = calc_entires(paginator, page_n)
    if request.method == "POST":
        if request.user.is_authenticated():
            if len(request.POST['text']) >= 5:
                Answer.objects.create(author=request.user.profile,
                                      question=question,
                                      text=request.POST['text'])
        else:
            return redirect('/login?rr=' + request.get_full_path())
    else:
        return render(request, 'question.html', {
            'question': question,
            'tags': getBestTags(), 'logged_in' : bool(random.randrange(1)),
            'answers': paginator.page(page_n),
            'entries' : entires,
            'bests' : getBestUsers(),
            })


def ask(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            form = QuestionForm(request.POST)
            if form.is_valid():
                tags_str = form.cleaned_data['tags']
                tags_names = tags_str.split(',')
                q = Question.objects.create(author=request.user.profile, title=form.cleaned_data['title'], text=request.POST['text'])
                for tag_name in tags_names:
                    try:
                        tag = Tag.objects.get(name=tag_name)
                        tag.rating += 1
                        tag.save()
                    except (ObjectDoesNotExist):
                        tag = Tag.objects.create(name=tag_name, rating=1)
                    q.tags.add(tag)
                return redirect('/question/' + str(q.id) + '/1')
            else:
                return render(request, 'ask.html', {
                    'tags': getBestTags(),
                    'bests': getBestUsers(),
                    'form': form,
                    })
        else:
            return render(request, 'ask.html', {
                'tags': getBestTags(),
                'bests': getBestUsers(),
                })
    else:
        return redirect('/login?rr=' + request.get_full_path())


def login_view(request):
    if not request.user.is_authenticated():
        if request.method == "POST":
            username = request.POST['login']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                   # if request.GET.get('redirect') is not None:
                    return redirect(request.GET.get('rr', '/'))
                    #else:
                     #   return redirect('/')
                else:
                    return render(request, 'login.html', {
                        'tags': getBestTags(),
                        'bests': getBestUsers(),
                        'message': 'Account is disabled)',
                        'username': username,
                        'password': password,
                        })
            else:
                return render(request, 'login.html', {
                    'tags': getBestTags(),
                    'bests': getBestUsers(),
                    'message': 'Wrong credentials!',
                    'username': username,
                    'password': password,
                    })
        else:
            return render(request, 'login.html', {
                'tags': getBestTags(),
                'bests': getBestUsers(),
                })
    else:
        return redirect('/')


def signin(request):
    if not request.user.is_authenticated():
        if request.method == "POST":
            username = request.POST['login']
            password = request.POST['password']
            email = request.POST['email']
            form = RegistrationForm(request.POST)
            try:
                if form.is_valid():
                        u = User.objects.create_user(username, email, password)
                        Profile.objects.create(avatar='/avs/usr.png', user=u)
                        user = authenticate(username=username, password=password)
                        login(request, user)
                        return redirect('/')
                else:
                    return render(request, 'signin.html', {
                        'tags': getBestTags(),
                        'bests': getBestUsers(),
                        'form': form,
                        })
            except IntegrityError:
                return render(request, 'signin.html', {
                    'tags': getBestTags(),
                    'bests': getBestUsers(),
                    'form': form,
                    'message': 'Duplicate login!'
                    })
        else:
            return render(request, 'signin.html', {
                'tags': getBestTags(),
                'bests': getBestUsers(),
                })
    else:
        return redirect('/')


def settings(request):
    if request.user.is_authenticated():
        if request.method == "POST":
            form = SettingsForm(request.POST, request.FILES)
            try:
                if form.is_valid():
                    u = User.objects.get(pk=request.user.id)
                    p = u.profile
                    u.username = form.cleaned_data['login']
                    u.set_password(form.cleaned_data['password'])
                    u.email = form.cleaned_data['email']
                    u.save()
                    p.avatar = request.FILES['file']
                    p.save()
                    user = authenticate(username=u.username, password=form.cleaned_data['password'])
                    login(request, user)
                    return redirect('/')
                else:
                    return render(request, 'settings.html', {
                        'tags': getBestTags(),
                        'bests': getBestUsers(),
                        'form': form,
                        })
            except IntegrityError:
                return render(request, 'settings.html', {
                    'tags': getBestTags(),
                    'bests': getBestUsers(),
                    'form': form,
                    'message': 'Duplicate login!'
                    })
        else:
            form = SettingsForm({'login': request.user.username, 'email': request.user.email, 'password': '', 'file': request.user.profile.avatar})
            return render(request, 'settings.html', {
                'tags': getBestTags(),
                'bests': getBestUsers(),
                'form': form,
                })
    else:
        return redirect('/')


def like_question(request):
    if request.method == 'POST':
        pk = request.POST.get('id')
        type = int(request.POST.get('type'))
        try:
            ql = QuestionLike.objects.get(profile_id=request.user.profile.id, question_id=pk)
            if ql.type != type:
                ql.type = type
                ql.save()
                ql.question.rating += 2 * type
                ql.question.save()
        except:
            ql = QuestionLike.objects.create(profile_id=request.user.profile.id, question_id=pk, type=type)
            ql.question.rating += type;
            ql.question.save()

        content = json.dumps({'status': True, 'message': 'ok', 'new_rating': ql.question.rating})
        return HttpResponse(content, content_type='application/json')

def like_answer(request):
    if request.method == 'POST':
        pk = request.POST.get('id')
        type = int(request.POST.get('type'))
        try:
            al = AnswerLike.objects.get(profile_id=request.user.profile.id, answer_id=pk)
            if al.type != type:
                al.type = type
                al.save()
                al.answer.rating += 2 * type
                al.answer.save()
        except:
            al = AnswerLike.objects.create(profile_id=request.user.profile.id, answer_id=pk, type=type)
            al.answer.rating += type;
            al.answer.save()

        content = json.dumps({'status': True, 'message': 'ok', 'new_rating': al.answer.rating})
        return HttpResponse(content, content_type='application/json')


def right_answer(request):
    if request.method == 'POST':
        pk = request.POST.get('id')
        a = Answer.objects.get(pk=pk)
        try:
            right = Answer.objects.get(question_id=a.question.id, is_right=True)
            right.is_right = False
            right.save()
        except:
            n = 10
        a.is_right = True
        a.save()
        content = json.dumps({'status': True, 'message': 'ok'})
        return HttpResponse(content, content_type='application/json')


@csrf_exempt
def hellodjango(request):
    # getting GET
    get_d = request.GET
    get_body = ['%s=%s<br>' % (key, value) for key, value in get_d.iteritems()]
    # getting POST
    post_d = request.POST
    post_body = ['%s=%s<br>' % (key, value) for key, value in post_d.iteritems()]
    #building response body 
    response_body = ['Hello world!<br>', '*' * 50 + '<br>GET:<br>'] + \
                    get_body + \
                    ['<br>' + '*' * 50 + '<br>POST:<br>'] + \
                    post_body
    return HttpResponse(response_body)
