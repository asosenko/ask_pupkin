from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from main.models import *
from django.contrib.auth.models import User
from django.db.models import Min, Max
from faker.frandom import random
from faker.lorem import sentence, sentences, words
from mixer.fakers import get_username, get_email
from pprint import pformat

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--users',
            action='store',
            dest='users',
            default=0,
        ),
        make_option('--questions',
            action='store',
            dest='questions',
            default=0,
        ),
        make_option('--answers',
            action='store',
            dest='answers',
            default=0,
        ),
         make_option('--tags',
            action='store',
            dest='tags',
            default=0,
        ),
    )

    def handle(self, *args, **options):
        names = {}
        while(len(names.keys()) < int(options['users'])):
            names[get_username(length=30)] = 1

        for name in names.keys():
            u = User.objects.create(username=name, email=get_email())
            p = Profile.objects.create(user=u, rating=random.randint(0, 51))

        p_min = Profile.objects.all().aggregate(Min('id'))['id__min']
        p_max = Profile.objects.all().aggregate(Max('id'))['id__max']

        for i in range(0, int(options['tags'])):
            t = Tag.objects.create(name=words(1)[0])

        t_min = Tag.objects.all().aggregate(Min('id'))['id__min']
        t_max = Tag.objects.all().aggregate(Max('id'))['id__max']


        for i in range(0, int(options['questions'])):
            q = Question.objects.create(author_id=random.randint(p_min, p_max),
                    title=(sentence())[:59], text=sentences(3),
                    rating=random.randint(0, 166), is_answered=random.randint(0,2))
            for j in range(1, random.randint(2, 5)):
                q.tags.add(Tag.objects.get(pk=random.randint(t_min, t_max)))

        q_min = Question.objects.all().aggregate(Min('id'))['id__min']
        q_max = Question.objects.all().aggregate(Max('id'))['id__max']

        for i in range(0, int(options['answers'])):
            a = Answer.objects.create(author_id=random.randint(p_min, p_max),
                    question_id=random.randint(q_min, q_max), text=sentences(3))

        for q in Question.objects.all():
            if q.is_answered == True and q.answer_set.count() > 0:
                q.answer_set.all()[0].is_right = True
            else:
                q.is_answered = False
            q.save()

