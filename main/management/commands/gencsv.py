from django.core.management.base import BaseCommand, CommandError
from optparse import make_option
from main.models import *
from django.contrib.auth.models import User
from django.db.models import Min, Max
from faker.frandom import random, bs_noun
from faker.lorem import sentence, sentences, words
from mixer.fakers import get_username, get_email, get_firstname, get_lastname
from pprint import pformat
from datetime import datetime, timedelta
import csv
import os

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--users',
            action='store',
            dest='users',
            default=0,
        ),
        make_option('--profiles',
            action='store',
            dest='profiles',
            default=0,
        ),
        make_option('--questions',
            action='store',
            dest='questions',
            default=0,
        ),
        make_option('--answers',
            action='store',
            dest='answers',
            default=0,
        ),
        make_option('--tags',
            action='store',
            dest='tags',
            default=0,
        ),
        make_option('--tags-questions',
            action='store',
            dest='tags-questions',
            default=0,
        ),
    )


    def handle(self, *args, **options):
        usr_n = int(options['users'])
        prf_n = int(options['profiles'])
        qst_n = int(options['questions'])
        ans_n = int(options['answers'])
        tag_n = int(options['tags'])
        t_a_n = int(options['tags-questions'])
        directory = os.path.dirname(os.path.realpath(__file__))
        if usr_n > 0:
            password='pbkdf2_sha256$12000$6xMbqmS6zSgm$tFjoUz8iBYvB5Tlng9QQB+nDSUy7qSONlOm3S5Bcegw='
            names = {}
            while len(names.keys()) < usr_n:
                names[get_username(length=30)] = 1
            fileUsr = open(directory + '/usr.csv', 'wb')
            writerUsr = csv.writer(fileUsr, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for name in names:
                writerUsr.writerow([
                    0,
                    password,
                    datetime.today().replace(microsecond=0) + timedelta(0, random.randint(0, 1e5)),
                    0,
                    name,
                    get_firstname(),
                    get_lastname(),
                    get_email(),
                    0,
                    0,
                    datetime.today().replace(microsecond=0)])

        if prf_n > 0:
            filePrf = open(directory + '/prf.csv', 'wb')
            writerPrf = csv.writer(filePrf, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(int(args[0]), int(args[1]) + 1):
                writerPrf.writerow([
                    0,
                    i, #user_id
                    random.randint(0, 100),
                    'avs/ava' + str(random.randint(0, 6)) + '.png'])

        if qst_n > 0:
            fileQst = open(directory + '/qst.csv', 'wb')
            writerQst = csv.writer(fileQst, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(0, qst_n):
                writerQst.writerow([
                    0,
                    random.randint(int(args[0]), int(args[1])), #profile_id
                    (sentence())[:59],
                    sentences(3),
                    random.randint(0, 666),
                    datetime.today().replace(microsecond=0),
                    0])

        if ans_n > 0:
            fileAns = open(directory + '/ans.csv', 'wb')
            writerAns = csv.writer(fileAns, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(0, ans_n):
                writerAns.writerow([
                    0,
                    random.randint(int(args[0]), int(args[1])),#profile_id
                    random.randint(int(args[2]), int(args[3])),#question_id
                    sentences(3),
                    random.randint(0, 666),
                    0,
                    datetime.today().replace(microsecond=0)])

        if tag_n > 0:
            fileTag = open(directory + '/tag.csv', 'wb')
            writerTag = csv.writer(fileTag, lineterminator='\n', quoting=csv.QUOTE_ALL)
            tag_names = {}
            while len(tag_names.keys()) < tag_n:
                tag_names[(words(2)[0] + words(2)[1])[:random.randint(2, 8)]] = 1
            for tag_name in tag_names.keys():
                writerTag.writerow([
                    0,
                    tag_name,
                    random.randint(0, 666)])

        if t_a_n > 0:
            fileT_A = open(directory + '/t_a.csv', 'wb')
            writerT_A = csv.writer(fileT_A, lineterminator='\n', quoting=csv.QUOTE_ALL)
            tas = {}
            while len(tas.keys()) < t_a_n:
                tas[(random.randint(int(args[0]), int(args[1])), random.randint(int(args[2]), int(args[3])))] = 1

            for i, j in tas.keys():
                writerT_A.writerow([
                    0,
                    i,  #question_id
                    j   #tag_id
                ])




