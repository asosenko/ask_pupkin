from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User)
    rating = models.IntegerField(default=0)
    avatar = models.ImageField(null=True, upload_to='avs')


class Tag(models.Model):
    name = models.CharField(max_length=60)
    rating = models.IntegerField(default=0)


class Question(models.Model):
    author = models.ForeignKey(Profile)
    tags = models.ManyToManyField(Tag)
    title = models.CharField(max_length=60)
    text = models.TextField()
    rating = models.IntegerField(default=0)
    is_answered = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now_add=True)


class Answer(models.Model):
    author = models.ForeignKey(Profile)
    question = models.ForeignKey(Question)
    text = models.TextField()
    rating = models.IntegerField(default=0)
    is_right = models.BooleanField(default=False)
    date_added = models.DateTimeField(auto_now_add=True, null=True)


class QuestionLike(models.Model):
    profile = models.ForeignKey(Profile)
    question = models.ForeignKey(Question)
    type = models.IntegerField(default=1)


class AnswerLike(models.Model):
    profile = models.ForeignKey(Profile)
    answer = models.ForeignKey(Answer)
    type = models.IntegerField(default=1)

