from cgi import parse_qs, escape

def application(environ, start_response):
   #getting GET
   get_d = parse_qs(environ['QUERY_STRING'])
   get_body = ['%s=%s\n' % (key, value) for key, value in get_d.iteritems()]
   #getting POST
   post_d = parse_qs(environ['wsgi.input'].read(int(environ.get('CONTENT_LENGTH', 0))))
   post_body = ['%s=%s\n' % (key, value) for key, value in post_d.iteritems()]
   #building response body 
   response_body = ['Hello world!\n', '*' * 50 + '\nGET:\n'] + \
                   get_body + \
                   ['\n'+'*' * 50 + '\nPOST:\n'] + \
                   post_body
   #content length
   content_length = 0
   for s in response_body:
      content_length += len(s)
   #building response
   status = '200 OK'
   response_headers = [('Content-Type', 'text/plain'),
                  ('Content-Length', str(content_length))]
   start_response(status, response_headers)
   return response_body
